import { RateLimiterMemory } from "rate-limiter-flexible";
import debug from 'debug';
import JwksRateLimitError from '../errors/JwksRateLimitError.js';

export default function(client, { jwksRequestsPerMinute = 10 }) {
  const logger = debug('jwks');
  const getSigningKey = client.getSigningKey;

  const opts = {
    points: jwksRequestsPerMinute,
    duration: 60,
  };

  const limiter = new RateLimiterMemory(opts);
  logger(`Configured rate limiting to JWKS endpoint at ${jwksRequestsPerMinute}/minute`);

  return (kid, cb) => {
    limiter.consume('app',1).then((rateLimiterRes) => {
      logger('Requests to the JWKS endpoint available for the next minute:', rateLimiterRes.remainingPoints);
      return getSigningKey(kid, cb);
    }).catch(() => {
      logger('Too many requests to the JWKS endpoint');
      return cb(new JwksRateLimitError('Too many requests to the JWKS endpoint'));
    });
  };
}
